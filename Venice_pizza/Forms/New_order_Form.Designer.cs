﻿namespace Venice_pizza
{
    partial class New_order_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.new_piz_btn = new System.Windows.Forms.Button();
            this.check_in_debt = new System.Windows.Forms.CheckBox();
            this.Ok_btn = new System.Windows.Forms.Button();
            this.price_txt = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // new_piz_btn
            // 
            this.new_piz_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.new_piz_btn.Location = new System.Drawing.Point(10, 10);
            this.new_piz_btn.Name = "new_piz_btn";
            this.new_piz_btn.Size = new System.Drawing.Size(117, 40);
            this.new_piz_btn.TabIndex = 0;
            this.new_piz_btn.Text = "Еще пиццу! :)";
            this.new_piz_btn.UseVisualStyleBackColor = true;
            this.new_piz_btn.Click += new System.EventHandler(this.new_piz_btn_Click);
            // 
            // check_in_debt
            // 
            this.check_in_debt.AutoSize = true;
            this.check_in_debt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.check_in_debt.Location = new System.Drawing.Point(14, 60);
            this.check_in_debt.Name = "check_in_debt";
            this.check_in_debt.Size = new System.Drawing.Size(149, 20);
            this.check_in_debt.TabIndex = 1;
            this.check_in_debt.Text = "Отсрочка платежа";
            this.check_in_debt.UseVisualStyleBackColor = true;
            this.check_in_debt.CheckedChanged += new System.EventHandler(this.check_in_debt_CheckedChanged);
            // 
            // Ok_btn
            // 
            this.Ok_btn.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Ok_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Ok_btn.Location = new System.Drawing.Point(133, 9);
            this.Ok_btn.Name = "Ok_btn";
            this.Ok_btn.Size = new System.Drawing.Size(100, 40);
            this.Ok_btn.TabIndex = 2;
            this.Ok_btn.Text = "Готово";
            this.Ok_btn.UseVisualStyleBackColor = true;
            this.Ok_btn.Click += new System.EventHandler(this.Ok_btn_Click);
            // 
            // price_txt
            // 
            this.price_txt.AutoSize = true;
            this.price_txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.price_txt.Location = new System.Drawing.Point(248, 22);
            this.price_txt.Name = "price_txt";
            this.price_txt.Size = new System.Drawing.Size(47, 15);
            this.price_txt.TabIndex = 4;
            this.price_txt.Text = "label1";
            // 
            // New_order_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(411, 428);
            this.Controls.Add(this.price_txt);
            this.Controls.Add(this.Ok_btn);
            this.Controls.Add(this.check_in_debt);
            this.Controls.Add(this.new_piz_btn);
            this.Name = "New_order_Form";
            this.Text = "Новый заказ";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button new_piz_btn;
        private System.Windows.Forms.CheckBox check_in_debt;
        private System.Windows.Forms.Button Ok_btn;
        private System.Windows.Forms.Label price_txt;
    }
}