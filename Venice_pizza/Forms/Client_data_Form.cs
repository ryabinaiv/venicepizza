﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Venice_pizza
{
    public partial class Client_data_Form : Form
    {
        ClientController controller;
        IRepository repository;
        public Client_data_Form(int client_id, IRepository drt)
        {
            InitializeComponent();
            controller = new ClientController(client_id, drt);
            repository = drt;
            this.StartPosition = FormStartPosition.CenterScreen;
            this.BackColor = Color.White;
            this.Size = new Size(600, 600);
            this.AutoScroll = true;
            this.new_ord_btn.BackColor = Color.LawnGreen;
            this.pay_debt_btn.BackColor = Color.White;
            this.pay_debt_btn.Visible = false;
            this.Text = "Клиент " + client_id +" - \"" + controller.my_name() + "\"";
            this.command.Text = "Заказы клиента:";
            this.command.Font = new Font("Arial", 11, FontStyle.Regular);
            this.Load += Debt_Load;
            this.Load += Table_Load;          
        }

        private void Debt_Load(object sender, EventArgs e)
        {
            double debt_money = controller.get_debt_sum();
            if (debt_money != 0)
            {
                if (debt_money < 0)
                {
                    this.debt.Visible = true;
                    this.debt.Location = new Point(135, 25);
                    this.debt.Font = new Font("Arial", 10, FontStyle.Bold);
                    this.debt.Text = "Переплата клиента: " + Convert.ToString(debt_money * (-1)) + " руб.";
                    this.debt.ForeColor = Color.Green;
                    this.pay_debt_btn.Visible = false;
                }
                else
                {
                    this.debt.Visible = true;
                    this.debt.Location = new Point(250, 20);
                    this.debt.Font = new Font("Arial", 12, FontStyle.Bold);
                    this.debt.Text = "Клиент должен: " + Convert.ToString(debt_money) + " руб.";
                    this.debt.ForeColor = Color.Red;
                    this.pay_debt_btn.Visible = true;
                    if (!controller.can_order())
                    {
                        this.new_ord_btn.BackColor = Color.Red;
                        this.new_ord_btn.Enabled = false;
                    }
                }
            }
            else
            {
                this.debt.Visible = false;
                this.pay_debt_btn.Visible = false;
            }
        }

        private void Table_Load(object sender, EventArgs e)
        {
            if (controller.get_ords().Count == 0) { this.command.Text = "У клиента еще нет заказов"; }
            else { this.command.Text = "Заказы клиента:"; }
            int x = this.command.Location.X, y = this.command.Location.Y + 25;
            Label ord_str;
            foreach (Order ord in controller.get_ords())
            {
                ord_str = new Label();
                this.Controls.Add(ord_str);
                ord_str.Location = new Point(x, y); y += 20;
                ord_str.Font = new Font("Arial", 10, FontStyle.Regular);
                ord_str.AutoSize = true;
                if (ord.in_debt()) { ord_str.ForeColor = Color.Red; }
                else { ord_str.ForeColor = Color.Black; } //обновление цветов заказов после оплаты задолженности
                ord_str.Text = Convert.ToString(ord.get_date()) + " - Пицца в количестве " + ord.get_pizza_count() + "шт. Сумма заказа " + ord.get_price() + " руб.";
            }

        }
        private void new_ord_btn_Click(object sender, EventArgs e)
        {
            New_order_Form new_ord = new New_order_Form(repository, controller.my_id());
            new_ord.ShowDialog();
            if( new_ord.DialogResult == DialogResult.OK)
            {
                this.Debt_Load(new_ord, new EventArgs());
                this.Table_Load(new_ord, new EventArgs());
            }
        }

        private void pay_debt_btn_Click(object sender, EventArgs e)
        {
            Pay form = new Pay(controller.my_id(), repository);
            form.ShowDialog();
            if (form.DialogResult == DialogResult.OK)
            {
                this.Debt_Load(form, new EventArgs());
                this.Table_Load(form, new EventArgs());
            }
        }
    }
}
