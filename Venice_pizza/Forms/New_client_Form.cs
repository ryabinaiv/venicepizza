﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Venice_pizza
{
    public partial class New_client_Form : Form
    {
        Controller controller;
        public New_client_Form(Controller c)
        {
            controller = c;
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterScreen;
            this.BackColor = Color.White;
            this.Ok.Text = "Завершить";
        }

        private void enter_btn_Click(object sender, EventArgs e)
        {
            if (!controller.new_client(this.textBox.Text))
            { this.label1.Text = "Введите клиента!"; this.label1.ForeColor = Color.Red; }
            else { this.label1.Text = "Введите название/имя:"; this.label1.ForeColor = Color.Black; this.textBox.Text = ""; }
        }

        private void Ok_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
