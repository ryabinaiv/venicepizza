﻿namespace Venice_pizza
{
    partial class Client_list_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.new_clt_btn = new System.Windows.Forms.Button();
            this.command = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // new_clt_btn
            // 
            this.new_clt_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.new_clt_btn.Location = new System.Drawing.Point(12, 12);
            this.new_clt_btn.Name = "new_clt_btn";
            this.new_clt_btn.Size = new System.Drawing.Size(131, 40);
            this.new_clt_btn.TabIndex = 0;
            this.new_clt_btn.Text = "Добавить клиентов";
            this.new_clt_btn.Click += new System.EventHandler(this.new_clt_btn_Click);
            // 
            // command
            // 
            this.command.AutoSize = true;
            this.command.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.command.Location = new System.Drawing.Point(10, 60);
            this.command.Name = "command";
            this.command.Size = new System.Drawing.Size(133, 16);
            this.command.TabIndex = 0;
            this.command.Text = "Выберите клиента:";
            // 
            // Client_list_Form
            // 
            this.ClientSize = new System.Drawing.Size(584, 561);
            this.Controls.Add(this.command);
            this.Controls.Add(this.new_clt_btn);
            this.Name = "Client_list_Form";
            this.Text = "Пиццерия ВЕНЕЦИЯ";
            this.Load += new System.EventHandler(this.Client_list_Form_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button new_clt_btn;
        private System.Windows.Forms.Label command;
       // private System.Windows.Forms.Button btn;
    }
}