﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Venice_pizza
{
    public partial class Pay : Form
    {
        ClientController controller;
        public Pay(int id, IRepository drt)
        {
            InitializeComponent();
            controller = new ClientController(id, drt);
            this.BackColor = Color.White;
            this.StartPosition = FormStartPosition.CenterScreen;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var message = controller.pay_debt(this.money.Text);
            MessageBox.Show(message, "Результат оплаты", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.Close();
        }
    }
}
