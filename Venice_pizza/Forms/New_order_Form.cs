﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Venice_pizza
{
    public partial class New_order_Form : Form
    {
        private ClientController controller;
        IRepository repository;
        public New_order_Form(IRepository drt, int id)
        { 
            InitializeComponent();
            controller = new ClientController(id, drt);
            repository = drt;
            this.Text = "Заказ для \"" + controller.my_name() + "\"";
            this.StartPosition = FormStartPosition.CenterScreen;
            this.BackColor = Color.White;
            this.Size = new Size(600, 600);
            this.AutoScroll = true;
            this.new_piz_btn.BackColor = Color.Yellow;
            this.new_piz_btn.ForeColor = Color.DarkRed;
            this.Ok_btn.BackColor = Color.White;
            controller.clear_order();
            this.price_txt.ForeColor = Color.DarkRed;
            this.price_txt.Visible = false;
            this.Load += Table_Load;
        }
 
        private void Table_Load (object sender, EventArgs e)
        {
            Label str_piz; double all_price = 0;
            int x = this.check_in_debt.Location.X, y = this.check_in_debt.Location.Y + 30;
            if (controller.get_pizzas().Count == 0) { this.new_piz_btn.Text = "Добавить пиццу"; }
            else
            {
                this.new_piz_btn.Text = "Еще пиццу! :)";
                Label command = new Label(); command.Text = "Заказ:";
                this.Controls.Add(command); command.Location = new Point(x, y); y += 20;
                command.Font = new Font("Arial", 10, FontStyle.Bold);
            }
            
            foreach (Pizza piz in controller.get_pizzas())
            {
                str_piz = new Label();
                this.Controls.Add(str_piz);
                str_piz.Location = new Point(x, y); y += 20;
                str_piz.Font = new Font("Arial", 10, FontStyle.Regular);
                str_piz.AutoSize = true;
                str_piz.Text = "Пицца \"" + piz.pizza_name + "\" в количестве " + Convert.ToString(piz.count_of_pizza) + " шт., " + Convert.ToString(piz.pizza_price*piz.count_of_pizza) + " руб.";
                all_price += piz.pizza_price * piz.count_of_pizza;
            }
            if (all_price > 0) { this.price_txt.Text = "Итог: " + Convert.ToString(all_price) + " руб."; this.price_txt.Visible = true; }
        }

        private void new_piz_btn_Click(object sender, EventArgs e)
        {
            Pizza_Form pizza_data = new Pizza_Form(controller.my_id(), repository);
            pizza_data.ShowDialog();
            if(pizza_data.DialogResult == DialogResult.OK)
            {
                this.Table_Load(pizza_data, new EventArgs());
            }
        }

        private void Ok_btn_Click(object sender, EventArgs e)
        {
            controller.save_order(this.check_in_debt.Checked);
            this.Close();
        }

        private void check_in_debt_CheckedChanged(object sender, EventArgs e)
        {
            if (check_in_debt.Checked == true) this.check_in_debt.ForeColor = Color.Red;
            if (check_in_debt.Checked == false) this.check_in_debt.ForeColor = Color.Black;
        }
    }
}
