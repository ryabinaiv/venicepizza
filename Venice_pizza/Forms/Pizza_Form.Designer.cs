﻿namespace Venice_pizza
{
    partial class Pizza_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pizza = new System.Windows.Forms.TextBox();
            this.price = new System.Windows.Forms.TextBox();
            this.chose_count = new System.Windows.Forms.NumericUpDown();
            this.all_price = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cancel_btn = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.chose_count)).BeginInit();
            this.SuspendLayout();
            // 
            // pizza
            // 
            this.pizza.Location = new System.Drawing.Point(10, 40);
            this.pizza.Name = "pizza";
            this.pizza.Size = new System.Drawing.Size(162, 20);
            this.pizza.TabIndex = 0;
            this.pizza.TextChanged += new System.EventHandler(this.pizza_TextChanged);
            // 
            // price
            // 
            this.price.Location = new System.Drawing.Point(189, 40);
            this.price.Name = "price";
            this.price.Size = new System.Drawing.Size(120, 20);
            this.price.TabIndex = 1;
            this.price.TextChanged += new System.EventHandler(this.price_TextChanged);
            // 
            // chose_count
            // 
            this.chose_count.Location = new System.Drawing.Point(10, 86);
            this.chose_count.Name = "chose_count";
            this.chose_count.Size = new System.Drawing.Size(162, 20);
            this.chose_count.TabIndex = 2;
            this.chose_count.ValueChanged += new System.EventHandler(this.chose_count_ValueChanged);
            // 
            // all_price
            // 
            this.all_price.Location = new System.Drawing.Point(189, 86);
            this.all_price.Name = "all_price";
            this.all_price.Size = new System.Drawing.Size(120, 20);
            this.all_price.TabIndex = 4;
            this.all_price.TextChanged += new System.EventHandler(this.textBox3_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(165, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Начните ввод названия пиццы:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(186, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Цена за одну:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 70);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Количество:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(186, 70);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(34, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Итог:";
            // 
            // cancel_btn
            // 
            this.cancel_btn.Location = new System.Drawing.Point(83, 119);
            this.cancel_btn.Name = "cancel_btn";
            this.cancel_btn.Size = new System.Drawing.Size(75, 35);
            this.cancel_btn.TabIndex = 9;
            this.cancel_btn.Text = "Отмена";
            this.cancel_btn.UseVisualStyleBackColor = true;
            this.cancel_btn.Click += new System.EventHandler(this.cancel_btn_Click);
            // 
            // button1
            // 
            this.button1.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.button1.Location = new System.Drawing.Point(164, 119);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(83, 35);
            this.button1.TabIndex = 10;
            this.button1.Text = "OK";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Pizza_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(323, 165);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.cancel_btn);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.all_price);
            this.Controls.Add(this.chose_count);
            this.Controls.Add(this.price);
            this.Controls.Add(this.pizza);
            this.Name = "Pizza_Form";
            this.Text = "Больше пиццы Богу пиццы!";
            ((System.ComponentModel.ISupportInitialize)(this.chose_count)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox pizza;
        private System.Windows.Forms.TextBox price;
        private System.Windows.Forms.NumericUpDown chose_count;
        private System.Windows.Forms.TextBox all_price;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button cancel_btn;
        private System.Windows.Forms.Button button1;
    }
}