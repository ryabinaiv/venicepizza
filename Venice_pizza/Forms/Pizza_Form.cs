﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Venice_pizza
{
    public partial class Pizza_Form : Form
    {
        ClientController controller;
        public Pizza_Form(int clt_id, IRepository drt)
        {
           // string return_piz;
           // decimal return_count;
            InitializeComponent();
            controller = new ClientController(clt_id, drt);
            this.BackColor = Color.White;
            this.StartPosition = FormStartPosition.CenterScreen;
            this.chose_count.Value = 1;
            this.button1.BackColor = Color.White;
            this.cancel_btn.BackColor = Color.White;
            this.price.Enabled = false;
            this.all_price.Enabled = false;
            AutoCompleteStringCollection source = new AutoCompleteStringCollection()
            {
            "Мясная",
            "Морская",
            "Гавайская",
            "Сладкая",
            "Вегетарианская"
            };
            pizza.AutoCompleteCustomSource = source;
            pizza.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            pizza.AutoCompleteSource = AutoCompleteSource.CustomSource;
            
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void pizza_TextChanged(object sender, EventArgs e)
        {
            double pizza_price = controller.get_one_piz_price(pizza.Text);
            this.price.Text = Convert.ToString(pizza_price);
            this.chose_count.Value = 1;
            this.all_price.Text = Convert.ToString(Convert.ToDecimal(pizza_price) * chose_count.Value);
        }

        private void chose_count_ValueChanged(object sender, EventArgs e)
        {
            double pizza_price = controller.get_one_piz_price(pizza.Text);
            this.all_price.Text = Convert.ToString(Convert.ToDecimal(pizza_price) * chose_count.Value);
        }

        private void price_TextChanged(object sender, EventArgs e){}

        private void cancel_btn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            controller.save(pizza.Text, chose_count.Value);
            this.Close();
        }
    }
}
