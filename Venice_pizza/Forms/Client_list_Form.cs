﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Venice_pizza
{
    public partial class Client_list_Form : Form
    {
        Controller controller;
        public Client_list_Form(IRepository drt)
        {
            controller = new Controller(drt);
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterScreen;
            this.BackColor = Color.White;
            this.new_clt_btn.BackColor = Color.White;
            this.AutoScroll = true;
            this.Load += Client_list_Form_Load;
        }
        public void Client_list_Form_Load(object sender, EventArgs e)
        {
            Dictionary<int,string> id_names = controller.get_clients_id_name();
            if (id_names.Count == 0) { this.command.Text = "Клиенты отсутствуют"; }
            else { this.command.Text = "Выберите клиента:"; }
            int i = 0;
            foreach (var item in id_names)
            {
                Button btn = new Button();
                btn.BackColor = Color.LightYellow;
                btn.Location = new Point(10, 80 + 25 * i++);
                btn.Size = new Size (565, 27);
                btn.TextAlign = ContentAlignment.BottomLeft;
                btn.Text = item.Key + " - \"" + item.Value + "\"";
                btn.Font = new Font("Arial", 10, FontStyle.Regular);
                btn.Click += elementClick;
                btn.Tag = item.Key;
                this.Controls.Add(btn);
            }
        }
        private void new_clt_btn_Click(object sender, EventArgs e)
        {
            New_client_Form next_form = new New_client_Form(controller);
            next_form.ShowDialog();
            if (next_form.DialogResult == DialogResult.OK)
                    { this.Client_list_Form_Load(next_form, new EventArgs()); }
        }

        void elementClick(object sender, EventArgs e)
        {
            var send = sender as Button;
            if (send == null)
            {
                return;
            }
            int id = (int)send.Tag;
            Client_data_Form clt_data = new Client_data_Form(id, controller.get_repository());
            clt_data.ShowDialog();
        }
    }
}
