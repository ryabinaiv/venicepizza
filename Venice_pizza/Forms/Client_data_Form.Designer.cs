﻿namespace Venice_pizza
{
    partial class Client_data_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.command = new System.Windows.Forms.Label();
            this.new_ord_btn = new System.Windows.Forms.Button();
            this.debt = new System.Windows.Forms.Label();
            this.pay_debt_btn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // command
            // 
            this.command.AutoSize = true;
            this.command.Location = new System.Drawing.Point(10, 60);
            this.command.Name = "command";
            this.command.Size = new System.Drawing.Size(0, 13);
            this.command.TabIndex = 0;
            // 
            // new_ord_btn
            // 
            this.new_ord_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.new_ord_btn.Location = new System.Drawing.Point(10, 10);
            this.new_ord_btn.Name = "new_ord_btn";
            this.new_ord_btn.Size = new System.Drawing.Size(115, 40);
            this.new_ord_btn.TabIndex = 1;
            this.new_ord_btn.Text = "Создать заказ";
            this.new_ord_btn.UseVisualStyleBackColor = true;
            this.new_ord_btn.Click += new System.EventHandler(this.new_ord_btn_Click);
            // 
            // debt
            // 
            this.debt.AutoSize = true;
            this.debt.Location = new System.Drawing.Point(240, 20);
            this.debt.Name = "debt";
            this.debt.Size = new System.Drawing.Size(0, 13);
            this.debt.TabIndex = 2;
            // 
            // pay_debt_btn
            // 
            this.pay_debt_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.pay_debt_btn.Location = new System.Drawing.Point(131, 10);
            this.pay_debt_btn.Name = "pay_debt_btn";
            this.pay_debt_btn.Size = new System.Drawing.Size(115, 40);
            this.pay_debt_btn.TabIndex = 3;
            this.pay_debt_btn.Text = "Оплатить долг";
            this.pay_debt_btn.UseVisualStyleBackColor = true;
            this.pay_debt_btn.Click += new System.EventHandler(this.pay_debt_btn_Click);
            // 
            // Client_data_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(490, 375);
            this.Controls.Add(this.pay_debt_btn);
            this.Controls.Add(this.debt);
            this.Controls.Add(this.new_ord_btn);
            this.Controls.Add(this.command);
            this.Name = "Client_data_Form";
            this.Text = "Client_data_Form";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label command;
        private System.Windows.Forms.Button new_ord_btn;
        private System.Windows.Forms.Label debt;
        private System.Windows.Forms.Button pay_debt_btn;
    }
}