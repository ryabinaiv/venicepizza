﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Venice_pizza
{
    public class Controller
    {
        static IRepository drt;
        private static bool create_dir;
        public Controller (IRepository _drt) {
            if (!create_dir)
            {
                drt = _drt;
                create_dir = !create_dir;
            }
        }
        public IRepository get_repository () { return drt; }
        public Dictionary<int,string> get_clients_id_name () { return drt.get_clients_id_name(); }
        public bool new_client(string name) { if (name != "") { drt.add_client(name); return true; } return false; }
        public int get_clients_count () { return drt.get_clt_count(); }
        public Client client_by_id(int id) { return drt.client_by_id(id); }
    }
    public class ClientController
    {
        private Client clt;
        private static List<Pizza> now_ord;
        public ClientController (int clt_id, IRepository drt)
        {
            clt = new Controller(drt).client_by_id(clt_id);
        }
        public double get_debt_sum() { return clt.get_debt_sum(); }
        public bool can_order() { return clt.can_order(); }
        public List<Order> get_ords() { return clt.get_orders();}
        public List<Pizza> get_pizzas() { return now_ord; }
        public string my_name() { return clt.get_name(); }
        public int my_id() { return clt.get_id(); }
        public double get_one_piz_price(string pizza) { return Pizza.get_one_piz_price(pizza); }
        public string pay_debt(string money)
        {
            double value = 0;
            try { value = Convert.ToDouble(money); }
            catch { return "Некорректная сумма платежа!"; }
            if (value < 0) return "Некорректная сумма платежа!"; ;
            clt.pay_for_debt(value);
            return "Оплата прошла успешно!";
        }
        public void clear_order() { now_ord = new List<Pizza>(); }
        public void save(string piz, decimal count)
        {
            if (Pizza.have_this_type(piz) && count > 0)
            {
                Pizza new_piz = new Pizza(piz, Convert.ToInt16(count));
                now_ord.Add(new_piz);
            }
        }
        public void save_order(bool debt)
        {
            if (now_ord.Count == 0) return;
            Order ord = new Order(now_ord, debt);
            this.clear_order(); 
            clt.add_order(ord);
        }
    }
}
