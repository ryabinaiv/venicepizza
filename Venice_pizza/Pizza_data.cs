﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Venice_pizza
{  

    public class Pizza
    {
        static private Dictionary<string, double> types = new Dictionary<string, double>
        {
        {"Мясная", 270},
        {"Морская", 300},
        {"Сладкая", 280}, 
        {"Гавайская", 280},
        {"Вегетарианская", 250}
        };
        public string pizza_name;
        public double pizza_price;
        public int count_of_pizza;
        public Pizza(string name) { pizza_name = name; pizza_price = types[name]; count_of_pizza = 1; }
        public Pizza(string name, int count) { pizza_name = name; pizza_price = types[name]; count_of_pizza = count; }
        static public double get_one_piz_price(string piz) { if (types.ContainsKey(piz)) return types[piz]; return 0; }
        static public bool have_this_type(string piz) { if (types.ContainsKey(piz)) return true;  return false; }
    }

    public class Order
    {
        private List<Pizza> order_pizza;
        private DateTime date;
        private double price;
        private bool debt;
        private int pizza_count;
        public Order(List<Pizza> pizzas, bool in_debt)
        {
            order_pizza = pizzas;
            date = DateTime.UtcNow;
            debt = in_debt;
            price = 0; pizza_count = 0;
            foreach (Pizza p in order_pizza) { price += p.pizza_price * p.count_of_pizza; pizza_count += p.count_of_pizza; }
        }
        public DateTime get_date() { return date; }
        public double get_price() { return price; }
        public bool in_debt() { return debt; }
        public void change_debt() { debt = !debt; }
        public int get_pizza_count() { return pizza_count; }
    }

    public class Client
    {
        private int Id;
        private string name;
        private List<Order> client_orders;
        private double debt_sum; // сумма долга по заказам
        private int count_debt_ord;
        private DateTime early_date_debt; // дата раннего из неоплаченных заказов

        public Client(int id, string n)
        {
            client_orders = new List<Order>();
            Id = id;
            name = n;
            debt_sum = 0;
            count_debt_ord = 0;
            early_date_debt = new DateTime(1, 1, 1, 0, 0, 0);
        }
        public int get_id() { return Id; }
        public string get_name() { return name; }
        public List<Order> get_orders() { return client_orders; }
        public double get_debt_sum() { return debt_sum; }
        public DateTime get_early_date_debt() { return early_date_debt; }
        public bool can_order()
        {
            if (debt_sum <= 0) return true;
            DateTime today = DateTime.Now;
            if (today.Subtract(early_date_debt).Days >= 7) return false;
            return true;
        }
        public bool add_order(Order new_ord) //true - добавлен, false иначе
        {
            if (!can_order()) return false;
            client_orders.Add(new_ord);
            if (!new_ord.in_debt()) return true;
            debt_sum += new_ord.get_price();
            count_debt_ord += 1;
            if (early_date_debt == new DateTime(1, 1, 1, 0, 0, 0)) { early_date_debt = new_ord.get_date(); }
            return true;
        }
        public void pay_for_debt(double money)
        {
            debt_sum -= money;
            if (debt_sum > 0) return; //долг не погашен
            foreach (Order ord in client_orders) { if (ord.in_debt()) ord.change_debt(); }
            count_debt_ord = 0;
            early_date_debt = new DateTime(1, 1, 1, 0, 0, 0);
        }
    }
}