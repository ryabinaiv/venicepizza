﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Venice_pizza
{
    public interface IRepository
    {
        int generic_id();
        Client client_by_id(int clt_id);
        void add_client(string name);
        int get_clt_count();
        void clt_save(Client clt);
        Dictionary<int, string> get_clients_id_name();
    }


    class Directory : IRepository
    {
        private int actual_id;
        private List<Client> clients;
        private int clt_count;
        public Directory() { clients = new List<Client>(); clt_count = 0; actual_id = 1; }

        public void clt_save(Client clt) { }

        public int generic_id() { return actual_id++; }

        public Client client_by_id(int clt_id)
        {
            foreach (Client i in clients) { if (i.get_id() == clt_id) return i; }
            return null;
        }

        public void add_client(string new_name)
        {
            Client new_client = new Client(generic_id(), new_name);
            clients.Add(new_client);
            clt_count++;
        }
        public int get_clt_count() { return clt_count; }

        public Dictionary<int, string> get_clients_id_name()
        {
            Dictionary<int, string> names = new Dictionary<int, string>();
            foreach (Client i in clients) { names.Add(i.get_id(), i.get_name()); }
            return names;
        }
    }
}
